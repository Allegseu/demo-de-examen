# Pasos a seguir

1. Crear el proyecto
2. Instalar las librerias
3. Configurar el AndroidManifest.xml (meta-data, application,users-permission)
4. Crear el modelo
5. Extender el modelo con sugar
6. Crear los atributos del modelo
7. Generar los constructores
8. Generar los getters and setters
9. Diseñar la vista principal (.xml) poniendo los id a los elementos
10. Declarar las variables de los elementos que vamos a usar del xml
11. Relacionar las variables con los elementos del xml dentro del OnCreate
12. Crear la funcion de guardar (boton guardar)
13. Crear una nueva actividad en blanco
14. Diseñar la vista secundaria (.xml) que tendra el recyclerView con id
15. Declarar la variables para el recyclerview 
16. Relacionar la variable con los elementos del xml dentro del OnCreate
17. Crear la funcion de listar (boton listar) 
18. Crear la coleccion de Datos en secundaria
19. Asignamos el tipo de layout de los elementos que tendra el recycler view
20. Crear y Diseñar el layout para los elementos de lista (.xml) con sus ids
21. Crear el adaptador y configurar
22. Extender la clase con RecyclerView.Adapter<Adaptador.ViewHolder>
23. Implementar la clase y los métodos que solicita (solucion de errores: 4)
24. Declarar las variables en la clase ViewHolder para los elementos de la lista
25. Relacionar las variables con los elementos del layout xml dentro del ViewHolder
26. Crear la prodiedad tipo lista de la clase adaptador 
27. Generar su constructor
28. Modificar el metodo onCreateViewHolder del adaptador (2)
29. Agregar funcionalidad al metodo onBindViewHolder para mostrar los datos en la vista
30. Modificar el metodo getItemCount del adaptador (1)
31. Asignar el Adaptador creado al recycler view en la actividad secundaria
32. Crear la funcionalidad de eliminar dentro del metodo onBindViewHolder (Boton eliminar de cada elemento)
33. Crear la funcionalidad de editar dentro del metodo onBindViewHolder (Boton editar de cada elemento)